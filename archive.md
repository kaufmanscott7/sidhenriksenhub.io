---
layout: page
title: Archive
permalink: /archive/
---
<ul>
{% for post in site.posts %}
  {% assign currentdate = post.date | date: "%B %Y" %}
  {% if currentdate != date %}
    <br><li id="y{{currentdate}}"><b>{{ currentdate }}</b></li>
    {% assign date = currentdate %} 
  {% endif %}
    <li><a href="{{ post.url }}">{{ post.title }}</a></li>
{% endfor %}
</ul>


