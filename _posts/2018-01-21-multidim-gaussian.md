---
layout: post
title:  "Why most points in a high-dimensional Gaussian lie on a thin shell"
date:   2018-01-21 17:00:00 +0000
categories: datascience mathematics statistics
mathjax: true    
---

One of the most fascinating cases of how low-dimensional reasoning fails to generalise to higher dimensions is how distances behave in high dimensions. This is especially the case with high-dimensional Gaussians. With high-dimensional Gaussians, as it turns out, the vast majority of points lie on a shell around the centroid of the Gaussian. This seems completely counterintuitive since in one, two, and three dimensions, Gaussians are bell curves, blobs, and spheres, respectively.

![png](/pics/multidim_gaussian/output_1_1.png)

Figure: One- and two-dimensional Gaussian probability density functions.

Looking at these figures it seems obvious that the majority of points will lie close to the centre. This is undoubtedly true when we look at the marginal distributions. For example, suppose we take a high-dimensional Gaussian and we plot the distribution along only one variable - this will just be a Gaussian as in the above plot. So how is it that most points exist on a shell for high-dimensional Gaussians? I have a pretty intuitive proof for this (which is no doubt completely unoriginal).

Let us compute the distance from the origin for a given point in a high dimensional Gaussian. This is simply

$$
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\SD}{\mathrm{SD}}
d(\mathbf{x}) = \sqrt{\sum_j^M x_{j}^2},
$$

for an \\(M\\)-dimensional Gaussian. We will assume that the variables in \\(\mathbf{x}\\) are IID (i.e. they are all Gaussians with same mean and SD, 
\\(\E[x_j] = \E[X] \ \forall j, \Var[x_j] = \Var[X] \ \forall j\\)). As we increase the number of dimensions \\(M\\), we can see that the distance squared will increase linearly

$$
\E[d^2] = \E \left[ \sum_j^M x_j^2 \right] = \sum_j^M \E[x_j^2] = M \E[X^2].
$$

We can see that the variance of the squared distance also increases linearly

$$
\Var[d^2] = \Var\left[\sum_j^M x_j^2\right] = \sum_j^M \Var[x_j^2] = M\Var[X^2].
$$

To get the standard deviation, we simply take the square root,

$$
\SD[d^2] = \sqrt{M \Var[X^2]} = \sqrt{M}\SD[X^2].
$$

In other words, wheras the average distance of a multi-dimensional Gaussian increases linearly with the number of dimensions, the SD around the mean distance increases with the square root. Thus, the coefficient of variation - or the standard deviation relative to the mean - decreases monotonically as you increase the number of dimensions. In symbols,

$$
\lim_{M \to \infty} \frac{\SD[d^2]}{\E[d^2]} = 0.
$$

We can actually work out what this will be exactly. I won't go through the derivation here, but with the aforementioned assumptions, it can be shown that

$$
\E[d^2] = M \E[X^2] = M \Var[X],
$$

and

$$
\Var[d^2] = M\Var[X^2] = 2M\Var[X]^2 \Rightarrow \SD[d^2] = \sqrt{2M}\Var[X],
$$

and so the coefficient of variation is simply

$$
C_v = \frac{\sqrt2M\Var[X]}{M\Var[X]} = \frac{\sqrt{2M}}{M} 
= (2 M)^{1/2}M^{-1} = \sqrt{2} M^{-1/2} = \sqrt{\frac{2}{M}}.
$$

## Simulation in Python
If you're like me then you tend to prefer having the maths backed up with some simulations (I am not ashamed to say that this is because I am a far better programmer than I am a mathematician). So let's just confirm this derivation we did above.


```python
maxM = 100
N = 5000
Xs = np.random.randn(N,maxM)**2

Ds = np.zeros((N,maxM)) # distance squared
              
for m in range(maxM):
    Ds[:,m] = np.sum(Xs[:,:m],axis=1)

# Okay, now let's compute the means and SDs
meanDistance = np.mean(Ds,axis=0)
sdDistance = np.std(Ds,axis=0)
simulatedCoV = sdDistance/meanDistance
analyticalCoV = np.sqrt(2/np.arange(maxM))

fig,ax = plt.subplots(1,figsize=(6.5,5.5))
ax.scatter(np.arange(maxM),simulatedCoV,marker='o',c='k',s=60)
ax.plot(np.arange(maxM),analyticalCoV,'-',c='r',lw=3)
ax.set_xlim([-1,101])
ax.set_ylim([0,1.5])
ax.set_xlabel('Number of dimensions',fontsize=18)
ax.set_ylabel('Normalised standard deviation',fontsize=18)
ax.legend(['Analytical','Simulation'],fontsize=16)
_ = list(map(lambda x:x.set_fontsize(16),ax.get_xticklabels() + ax.get_yticklabels()))
```

![png](/pics/multidim_gaussian/output_3_1.png)


So what does that actually mean? Well, the above result shows that as we increase the number of dimensions, the distance we can expect to find ourselves from the mean decreases with the square root of the number of dimensions. Thus, as we move to higher and higher dimensions, we can expect to increasingly find ourselves a very small distance away from the average distance from the origin. In other words, this means that high-dimensional Gaussian points exist almost entirely on a narrow shell. 
