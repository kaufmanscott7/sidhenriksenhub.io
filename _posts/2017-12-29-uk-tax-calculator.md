---
layout: post
title:  "UK tax calculator"
date:   2017-12-29 12:00:00 +0000
categories: scotland
---
This is a bare bones tax calculator for Scotland and the rUK for 2017/18 and the proposed bands
for 2018/19.
This does not factor in any deductions you may be entitled or anything other than income tax and
national insurance contributions.

<iframe src="https://sidhenriksen.com/apps/taxcalc" width="100%" height="500px" frameBorder="0">
