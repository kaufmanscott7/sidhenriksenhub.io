---
layout: post
title: "Why autoencoders are no magic bullet"
date: 2017-10-20 07:30:00 +0100
categories: datascience neuroscience
---
I have been involved in a few different projects where autoencoders have been proposed.
It is a concept that comes up often enough that I have spent some time thinking about their utility,
why they're interesting, and why they're just a bit crap.

A lot of my thinking on this
has come from [Francois Chollet's excellent post](https://blog.keras.io/building-autoencoders-in-keras.html) 
about building autoencoders in 
Keras. He notes that a lot of people who are relatively
new to machine learning take a big interest in autoencoders because they overcome the fairly 
constrained limitations of linear dimensionality reduction techniques such as principal components
analysis (PCA). Specifically, the idea behind an autoencoder is that you build a neural net
whose objective function is to minimise the reconstruction error of its input. 
In other words, the neural
net takes some input, does a bunch of internal nonlinear transformations, and then tries to
reconstruct its original *inputs* (hence the name autoencoder). The hope is that the autoencoder 
will 
come up with an efficient representation which captures the underlying structure of the data.
In a sense, autoencoders are "self-supervised" as opposed to being merely unsupervised. 

<img src="/pics/autoencoders/autoencoders.png" alt="Autoencoder illustration" style="display: block; margin: auto">


The idea behind an autoencoder is shown in this schematic\: the input (an image) goes in to 
the network.
The weights of the network are adjusted so as to minimise the output reconstruction, i.e.
the network will try to make the output as similar to the input as possible. The resulting learned
features will generally capture some of the underlying structure in the dataset.

It's a really cool idea, and in some cases, it can actually work really well. 
An area that I find particularly
interesting is their application to images since it is closely related to what I did my PhD on.
When autoencoders are applied to images, the resulting features often look a lot like edges. Such
edge detectors are pretty ubiquitous in the brain, especially in primary visual cortex (or V1 for
short). This is presumably no accident, and instead likely reflects that neurons in V1 
(and autoencoders trained on images) have adapted
for the image statistics of the real world. 
In my view, autoencoders can be very insightful when
the data is unstructured (like image data, which is just a collection of pixels). In these
cases, autoencoders can pick up on underlying structure. Their current practical
utility is probably limited to data visualisation of unstructured data and a small handful
of other applications.

If you or your organisation are still keen on applying autoencoders, I would add the following
caveats and pitfalls:

1. **Indiscriminately applying autoencoders.** As noted, autoencoders are good for unstructured
data problems since they allow you to learn features which potentially capture some aspect of the
underlying structure in the data. However, for structured datasets - i.e. where one field
contains an amount of money in GBP, another field contains the age of the person, and yet another
contains how many times a week a person does their grocery shopping - autoencoders are less
useful. They may still be able to pick out structure in the data, but their utility as a
data visualisation tool has pretty much disappeared.

2. **Data normalisation**. Normalising the data is essential for applying autoencoders since
autoencoders try to minimise the reconstruction error. If you have data on widely different
scales (e.g. 1000s of pounds vs counts in the range 0-10), then the reconstruction error
will be dominated by the large numbers in the data. Normalising is essential to making things
work, though it may still be a complicated, uninterpretable mess..

3. **Use in conjunction with supervised learning**. This is an interesting idea, which
as Francois Chollet points out in the aforementioned article, was all the rage for a brief
period of time a few years ago. Nowadays, there are alternative techniques for training
deep nets. The key problem in combining supervised and unsupervised approaches
is that the features which capture variability in the data (i.e. are useful for minimising
reconstruction error)
may not be useful for discriminating between two classes. The plot below illustrates this.
The black and red dots represent members of different classes. 

![Non-separable with first prinicipal component](/pics/autoencoders/nonseparable.png)

Note how the two features
are correlated with one another. Any sensible unsupervised dimensionality reduction technique 
would extract this direction (shown by the blue line) as the primary axis. Indeed, this is
exactly what principal components analysis is doing. Barring overfitting, a well-functioning
autoencoder would extract the same axis of variation (autoencoders
*can* extract more interesting, nonlinear features, but they do not have to). However, this
axis is orthogonal to the axis of variation which is important for separating the two classes
(equivalently, it is parallel to the decision boundary). In this case, when we now project
the data on to the first principal component (right plot, above), we get a massive degree of
overlap between the two classes. Consequently, we are unable to reliably classify these
cases even though they are very much linearly separable.

That being said, there are cases where autoencoders can perform well,
and this is when they learn features that are meaningful to actually making the classification
in question. In other words, it is necessary that the primary sources of variability in the data
are related to the label in some meaningful way.
In our two-dimensional cartoon view, this would look like the following:

![Separable with first prinicipal component](/pics/autoencoders/separable.png)

The first principal component (blue line) is very similar to the one before, but now can be used to 
actually discriminate the two classes (right plot). This is because the first principal component
is orthogonal to the decision boundary, meaning that the principal component is useful for
determining which side of the decision boundary a data point lies.

I think in practice, we usually find ourselves somewhere between these two extremes. I am also not
sure how these ideas generalise to higher dimensions, but I suspect the broad principles will
still apply. In general, it is best to be careful about applying any unsupervised learning 
technique as a preprocessing step. Sometimes this may work very well, but other times you might
just be learning features that aren't important for the classification task.
