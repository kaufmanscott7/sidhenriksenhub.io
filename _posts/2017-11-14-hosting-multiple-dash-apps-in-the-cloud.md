---
layout : post
title : 'Hosting Dash apps in the cloud using Nginx and uWSGI'
date : 2017-11-14 22:00:00 +0100
categories : datascience
---

I've recently started doing a lot of work with Dash. Dash is a Python package by Plotly which
allows you to create interactive visusalisations (dashboards) easily. Dash sits on top of Flask,
and uses
the React JavaScript library to make slick, interactive elements. 

Here is a Dash app I made to accompany a paper we're currently preparing. It's a fairly esoteric
visualisation, but you can click on the individual points and browse through a bunch of cells
that we have recorded from in primary visual cortex and machine learning models we fit to them.
Dash and related technologies
greatly streamline the process of developing data products and I'm a huge fan.

<center>
<iframe src="https://sidhenriksen.com/apps/corrboosting" width="90%" height="950px" frameBorder="0" scrolling='no'></iframe>
</center>

In the last few weeks, I've been trying to work out how to host multiple Dash apps on AWS. Currently
I only know of one way of doing this, which is through [multi-page apps](https://plot.ly/dash/urls).
The above app is hosted through a multi-page app on AWS.
In principle, you should be able to run this through the uWSGI Emperor, but I have been having
some issues with that. Currently, multi-page apps work really well and is officially documented
by Plotly, so I would definitely recommend sticking with that.

Regarding AWS, in case
you don't know, Amazon gives you a year worth of a fairly modest server for free. After that it's
currently at something like $100 a year. I would recommend people to take advantage of the free
offer since it's really good practice
to get familiar with firing up AWS instances and doing work on them. Once you know how to use AWS,
it really opens your eyes to what you can do as a data scientist.

I ended up running with Nginx for my web server and uWSGI for serving the Python app. That means we
need to get Nginx and uWSGI to talk to each other. Luckily, that's not
all that difficult. There
are basically two approaches I've been experimenting with for hosting Dash apps, and aside from the
very last step, they are basically identical. I will assume you are working on AWS for this,
with Ubuntu 16.04 installed.

Disclaimer: the following is largely plagiarised from a bunch of different
sources online. I read a lot of them to come up with a basic understanding of how all of this sort
of links together. There are a large number of very similar walkthroughs regarding hosting Flask
apps, but I couldn't find one that was tailored for Dash, which is the purpose of this post.

# Nginx installation
Okay, so the first step is to set up Nginx. This is really easy and all you have to do is to use
the package manager.

    sudo apt-get install nginx

Done. If Nginx is working, you should be able to visit your host name in your URL and you should the
Nginx screen. If that's not the case, then you've probably not configured your server to allow
access. For AWS, you can do this in the dashboard by changing which IPs are allowed to access your
instance. You need to make sure you see the Nginx screen before you proceed.

# Optional: virtualenv installation and setup
In general, you want to have virtual environments set up. These are isolated Python environments
which allow you to have different versions of libraries installed for each environment. This allows
you to manage conflicting dependencies. So let's just set that up. I'm assuming you want to run
your apps in Python 3.

    sudo apt-get install pip3
    sudo pip3 install virtualenv
    mkdir myproject
    virtualenv myproject/venv


Simples. Note that even if you want Python 3, you will want to use "pip install" inside your
virtual environment. In order to activate your virtual environment and install the relevant packages

    source myproject/venv/bin/acitvate
    pip install -r requirements.txt
    deactivate

Here requirements.txt contains a list of packages you want to install (you can also just install
your packages as you would normally do, but it is good practice to include a requirements file).

# Installing uWSGI
If you're using a virtualenv, you want to install the uWSGI within that environment. So activate
your virtual environment as above and install uWSGI using pip.

    source myproject/venv/bin/activate
    pip install uwsgi
    deactivate
    
Excellent. If you're not using a virtualenv, just install uwsgi through pip without first activating
your virtualenv. Now we just need to configure Nginx and set up uWSGI.

# Configuring Nginx
Nginx communicates with uWSGI through a socket. A socket is just a node through which the two
programs can talk, using a particular protocol. We need to configure Nginx to allow this.

The first step is to create a file in /etc/nginx/sites-available. Call it something informative
(main.config or whatever). Then open this file in nano

    sudo nano /etc/nginx/sites-available/main.config    

And now we're going to set up some configs:

    server {
        listen 80;
        server_name myip myawshostname;
        root /var/www/html;

        location / {
            include_uwsgi params;
            uwgsi_pass unix:/var/tmp/main.sock;
        }
    }

Let's explain what's going on here. We're telling Nginx to listen to port 80, which is the default
HTTP port. We're then telling it the server name, i.e. through which URLs it should expect requests.
We then set the root directory (you can set this to be whatever you want; remember to actually
create the directory). If you want to host a website on your server, this is where you would put it.
My AWS instance just points to this website (e.g.
https://www.sidhenriksen.com) because I'm
lazy. Okay, and then we get to the uWSGI part. The first bit just tells us to use some default
uwsgi params, and the second bit (uwsgi_pass) lets us specify the socket with which Nginx will
expect to communicate with uWSGI. Go ahead and save that, then run:

    sudo ln -s /etc/nginx/sites-available/* /etc/nginx/sites-enabled
    sudo service nginx reload

The first bit just sets up some symbolic links between sties-available and sites-enabled so you
don't have to duplicate files. That should be you sorted. For good measure, you can add an
index.html file in /var/www/html/ to make sure that everything is set up properly (this is
optional).

    echo "Hello world!" > /var/www/html/index.html

If all is well, you should be able to visit your server and see "Hello world!" in your browser.

# Configuring uWSGI
So far we've installed Nginx and uWSGI and configured Nginx. Note that in the last step, we told
Nginx the socket it should communicate with uWSGI through. Now we need to configure uWSGI so that
it knows which Dash app to run and how to talk to Nginx (which is our web server, remember).
So this step is actually slightly more involved, but it's not too bad. We need to configure a
Flask entry point:

    from app import server as application

    if __name__ == "__main__":
       application.run()

Call the above file wsgi.py.
The app.py module we're referring to here would actually be your multi-page app. It is important
that when you create your app, you set the server variable to the server of your app. For example,

    import dash
    app = dash.Dash()
    server = app.server

The reason for this is that uWSGI needs access to the underlying Flask server.
It will also work
with a single-page app, but without a bit more tweaking, you won't be able to host multiple
single-page apps. I think the simplest way to host multiple apps is to simply use a multi-page one,
especially because this process is documented/supported by Plotly.

Next, create your uWSGI config file. Call it app.ini

    [uwsgi]
    base=/var/www/myproject
    chdir=/home/ubuntu/myproject
    module = wsgi:application

    master = true
    processes = 4

    uid=ubuntu
    gid=www-data

    virtualenv = /home/ubuntu/myproject/venv

    socket = /var/tmp/main.sock
    chmod-socket = 666
    vacuum = true

    die-on-term = true

At long last, we can actually host your app. To run, first activate your virtual environment:

    source /home/ubuntu/myproject/venv/bin/activate
    uwsgi --ini app.ini

See if you can see your app in the browser. Hopefully you can and that is all there is to it
(congrats!). If not, you need to start debugging. The most useful thing to figure out what's
gone wrong is the Nginx error log (/var/log/nginx/error.log). Poke and prod the log and Google
the outputs. You really want to figure out this step before you proceed with running your app
as a daemon.

# Running uWSGI as a daemon (systemd)
This is the final step. We want this to run on our AWS instance (or whatever server you're using)
while we're not actually maintaining a session. Now, uWSGI actually comes with a mode called
Emperor which
allows you to control multiple apps (e.g. Flask, Dash, Django). However, for the multi-page app,
you're actually just managing one app so the uWSGI Emperor is not necessary. Instead, we can
use systemd to set up our visualisation as a service. This is actually incredibly straightforward.
All we do is create a systemd service file which will run uWSGI in the background. To do this,
create a file called something informative like myproject.uwsgi.service in /etc/systemd/system.
In that file, put the following:

    [Unit]
    Description=Put in something informative here
    After=network.target

    [Service]
    User=ubuntu
    Group=www-data
    WorkingDirectory=/home/ubuntu/myproject
    Environment="PATH=/home/ubuntu/myproject/venv/bin"
    ExecStart=/home/ubuntu/myproject/venv/bin/uwsgi --ini app.ini

    [Install]
    WantedBy=multi.user.target

Now, start this service up by running

    sudo service myproject.uwsgi start

And voila: your app is running as a daemon. Now, the cool thing is that you can just add more apps
to the multi-page app up to whatever sensible number your server can host.

This post has mostly been about how to get Nginx and uWSGI set up in order to host a Dash app.
The second step here (or first?) is to be able to create multi-page apps. Plotly documents that
process pretty well I think ([here the link is again for good measure](https://plot.ly/dash/urls)).

Note that once you've set up your web server, you really want to also set up SSL, especially if
you're going to be hosting stuff on your website. [LetsEncrypt](https://letsencrypt.org/) has a
complete walkthrough which is super easy to follow. You will need a domain name to do this (that
can be either a free one or a .com one). 