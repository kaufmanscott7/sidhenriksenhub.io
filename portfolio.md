---
layout: page
title: Portfolio
permalink: /portfolio/
---
This is a collection of projects I have done either in academia or in my spare time.

---

# Generating mechanistic models for predicting neuronal activity
For my PhD, I developed models of neurons in primary visual cortex of the primate brain.
For the resulting paper - currently under review - I developed an interactive visualisation.
The visualisation is completely web-based and is written in Dash.
Click on the image below to open the relevant page.

<center>
<a href="/portfolio/correlated-boosting.html">
<img src="/pics/corrboosting/corrboosting.jpg" style="width:90%;height:90%"></a>
</center>

<br>

---

# Predicting MMA fight outcomes
This is a series of mini-projects which started with building a web crawler to scrape
data from a website (Fightmetric.com). I then built a data processing pipeline which culminated
in a classifier that predicts which person will win the fight. Finally, I developed an app which
allows users to browse through matchups between any two fighters (real or imagined), view fighter
stats, and see the predicted fight outcome.

<center>
<a href="https://sidhenriksen.github.io/datascience/2017/11/23/predicting-mma-fight-outcomes.html   ">
<img src="/pics/mma/gsp_vs_bisping.jpg" style="width:90%;height:90%"></a>
</center>

<br>

---

# BEMtoolbox
As part of my PhD, I spent a lot of time modelling binocular neurons in primary visual cortex.
I built a Matlab toolbox for running these simulations, which provides a framework for quickly
prototyping ideas. It also features a number of optimisations and computational tricks for
efficiently running these simulations at scale.

<center>
<a href="https://github.com/sidhenriksen/BEMtoolbox">
<img src="/pics/bemtoolbox/bemviz.jpg" style="width:content-width;height:100%" alt="BEMtoolbox">
</a>
</center>