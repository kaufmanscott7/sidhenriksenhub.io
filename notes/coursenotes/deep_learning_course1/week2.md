---
layout: page
exclude: true
mathjax: true
---

<script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS-MML_HTMLorMML" type="text/javascript"></script>

## Logistic Regression as a Neural Network
This week introduces logistic regression as a foundational unit of a neural network (though most
modern neural nets use RELUs rather than sigmoid units)..
The [course](https://www.coursera.org/learn/neural-networks-deep-learning/) has a very helpful
Jupyter notebook. Andrew Ng's courses tend to target a very wide
audience, which means that the exercises can give a bit too much away. Therefore, the following
is basically done independently from that notebook using the notes from the lectures and my own
attempts at doing the derivations (where applicable).

Logistic regression models the probability of an example being a given class by a sigmoid

$$ \hat{y} = a = \frac{1}{1+\exp(-z)} $$

where \\(z = \mathbf{x}\mathbf{w}^\mathrm{T} + b \\). \\(\mathbf{x}\\) is our feature vector, and
\\(\mathbf{w}\\) is the weight vector. \\(b\\) represents the bias of the unit. We define the
sigmoid in Python below.

```python
import numpy as np
   
def sigmoid(x,w,b):
    '''
    Sigmoid function with weight parameters w and bias b.
    
    Parameters
    -----------
    x : array, input vector
    w : array, weight vector
    b : numeric, bias
    
    Returns
    ---------
    a : sigmoid-transformed inputs between 0 and 1
    '''
    
    z = np.dot(x,w) + b
    
    a = 1/(1+np.exp(-z))
    
    return a
```

# The chain rule
A quick recap of the chain rule, which is how we will eventually understand backprop. The chain rule is simply:

$$\frac{d J}{d v} = \frac{d J}{d u} \frac{d u}{d v}$$

The intuition for this is pretty straightforward: we can describe how a small change in v leads to
a change in J by describing how J changes with a small change in u, and then multiplying this by
how much u changes with a small change in v. Or more straightforwardly, we can see how the
numerator and the denominator cancel each other out. Technically, we should be using partial
derivative notation here when multiple variables simultaneously affect \\(J\\),

$$
\frac{\partial J}{\partial v} = \frac{\partial J}{\partial u} \frac{d u}{d v}.
$$

And then you just make sure no mathematician is looking and do the cancellation as before.

# Cost function for logistic regression
We will use the cross-entropy loss. Following from the course terminology, we will use the "loss"
to denote the particular loss, and the "cost" to denote the loss across the entire training set.
Thus, we define the loss to be

$$
\mathcal{L}(y,\hat{y}) = -\left(y\log \hat{y} +
(1-y) \log (1-\hat{y}) \right).
$$    

\\(y\\) and \\(\hat{y}\\) refer to the observed and predicted labels, respectively. In logistic
regression (and in most classification models), \\(\hat{y}\\) will generally be a probability
\\(0 < p < 1\\) (this will never be exactly 0 or 1 since the logistic function is only 0 and 1 in
the limit). We next define the cost over all training examples as

$$
\mathcal{C}(\mathbf{y},\mathbf{\hat{y}}) = 
\frac{1}{m}\sum_i^m \mathcal{L}(y^{(i)},\hat{y}^{(i)}).
$$

Here, \\(\mathbf{y}\\) and \\(\mathbf{\hat{y}}\\) refer to vectors of the observed and predicted
labels respectively. We define this in Python below.

```python
def cost_cross_entropy(y,yhat):
    '''
    Cross entropy cost function
    
    Parameters
    -----------
    y : array, actual class label (0 or 1)
    yhat : array, classification probability that y = 1 (in range [0,1])
    
    Returns
    --------
    c : cross-entropy cost (c > 0)
    
    '''
    c = -np.mean( y * np.log(yhat) + (1-y) * np.log(1-yhat))
    return c
```
The derivative of the loss function with respect to the model parameters is obtained by first
differentiating the loss \\(\mathcal{L}\\) with respect to
the sigmoid output. The output of the sigmoid is \\(a = \frac{1}{1+e^{-z}}\\) as above.

\begin{align}
\frac{d \mathcal{L}}{da} &= \frac{d}{da} \left( -y\log{a} +
  (1-y) \log{(1-a)} \right) \\\
  &= -\frac{y}{a} + \frac{1-y}{a-1}
\end{align}

This tells us how the cross-entropy loss changes for a particular classification example as we
change the value of the prediction. Next, we will show how the value of the prediction changes
as we change the parameters for the logistic function (which we will then chain together to
obtain how the loss changes as we change the parameters).

$$
\frac{\partial a}{\partial w_j} =  \frac{da}{dz}\frac{\partial z}{\partial w_j} = 
\frac{w_j e^{-z}}{(1+e^{-z})^2} = w_j a(1-a).
$$

This last step is a bit of a jump, but basically just involves some tedious algebra which I don't
go into.
Using the chain rule, we obtain the derivative of the loss with respect to the weight parameter.

$$ \frac{\partial \mathcal{L}}{\partial w_j} 
   = \frac{d\mathcal{L}}{da} \frac{\partial a}{\partial w_j}
   = ( -\frac{y}{a} + \frac{1-y}{1-a} ) ( w_j a(1-a)) $$
   
Note that this is essentially the backpropagation algorithm: propagating the error derivatives
throughout the network to the appropriate node. By multiplying the terms out, we get

\begin{align}
\frac{\partial \mathcal{L}}{\partial w_j}
&=  w_j ( - \frac{y}{a} + \frac{1-y}{1-a} ) a(1-a)) \\\
&= w_j ( -  y(1-a) + a(1-y) ) \\\
&= w_j (a-y)
\end{align}

If we do the same for the bias term, we get
    
$$ \frac{\partial \mathcal{L}}{db} = a-y $$

An alternative way of dealing with the bias term is to have a dummy column of all ones (i.e. a
feature which never changes value). This result shows how these two approaches are equivalent,
since for \\(j=0\\) (corresponding to the dummy column) \\(X_0^{(i)} = 1 \ \forall i \\). Let's
write this up in Python.

```python
def get_logistic_gradients(X,y,a):
    '''
    Gets the gradient for a logistic regression classifier
    given an input matrix X, an actual output y, and a
    predicted output a
    
    Parameters
    -----------
    X : array, N x M, observations by features
    y : array, 1 x N (or N x 1), labels
    a : array, 1 x N (or N x 1), predicted label probabilities
    
    Returns
    db : gradient for bias
    dW : gradient for weights
    
    '''
    N = X.shape[0]
    dz = a-y
    if dz.shape[0] != N:
        dz = dz.T
    
    assert dz.shape[0] == N, 'Error: a and y must have same number of rows as X'
    
    db = np.mean(dz)
    dW = X.T.dot(dz) / N
    
    return db,dW
```

Now we're in a position to implement logistic regression end-to-end. We will use the sklearn API-style to do this since it makes everything easier.


```python
class LogisticRegression:
   '''
   Logistic regression classifier class replicating the sklearn API.
   Implements fit and predict methods.

   Parameters
   -----------
   maxIterations : int, default 1000.
       Maximum number of learning iterations

   minImprovement : double, default 1e-5.
       Minimum improvement in cost from one iteration to the next before terminating.

   learningRate : double, default 0.1.
       Learning rate. Increase this to speed up training (too high may lead to diverging
       gradient).


   Attributes
   ----------
   coefs_ : array
      Weight coefficients

   bias_ : double
      Bias of the logistic unit

   '''
    
    def __init__(self,maxIterations=1000,minImprovement=1e-5,learningRate=0.1):
        self.maxIterations = maxIterations
        self.minImprovement = minImprovement
        self.learningRate = learningRate

    def predict_proba(self,X):
        yhat = sigmoid(X,self.coefs_,self.bias_)
        return yhat

    def predict(self,X):
        return np.double(self.predict_proba(X) > 0.5)
    
    def fit(self,X,y,verbose=False):
        # initialise
        self.coefs_ = np.random.rand(X.shape[1])
        self.bias_ = 0.0
        yhat = self.predict_proba(X)
        
        currentCost = cost_cross_entropy(y,yhat)
        
        for k in range(self.maxIterations):
            db,dW = get_logistic_gradients(X,y,yhat)
            self.coefs_ -= dW*self.learningRate
            self.bias_ -= db*self.learningRate
            yhat = self.predict_proba(X)
            newCost = cost_cross_entropy(y,yhat)
            dCost = currentCost-newCost
            if verbose:
                print('Current cost function value: %.4f (change: %.4f)'%(newCost,dCost))
                
            if dCost < self.minImprovement:
                print('Minimum improvement threshold reached. Breaking.')
                break
                
            currentCost = newCost
            
        return self
```

Let's generate some dummy training data and put this to the test. We will just generate
some Gaussians with different means.

```python
n,m= 500,2
X1 = np.random.randn(n,m)
X2 = np.random.randn(n,m)
for k in range(m):
    X1[:,k]+= np.random.rand()*5
    X2[:,k]+= np.random.rand()*5
    
X = np.vstack([X1,X2])
y = np.arange(1000)<500

classifier = LogisticRegression()
classifier.fit(X,y,verbose=True)

```
    Current cost function value: 0.8111 (change: 0.3119)
    Current cost function value: 0.6584 (change: 0.1528)
    Current cost function value: 0.5954 (change: 0.0630)
    ...
    Current cost function value: 0.1283 (change: 0.0000)
    Current cost function value: 0.1282 (change: 0.0000)
    Current cost function value: 0.1282 (change: 0.0000)


Okay, so that's finished running. Let's visualise the decision boundary to see if this thing works.
We can actually work this out analytically with logistic regression (the decision boundary
will be orthogonal to the weight vector), but we will just do it computationally for this since
when we move to nonlinear decision boundaries, we will not be able to solve this analytically.

```python
import matplotlib.pyplot as plt
fig,ax = plt.subplots(1,figsize=(7.5,6))
ax.scatter(X[y==0,0],X[y==0,1],marker='o',s=30,c='k')
ax.scatter(X[y==1,0],X[y==1,1],marker='o',s=30,c=[0.8,0.1,0.1])

z0 = np.linspace(np.min(X[:,0]),np.max(X[:,0]),100)
z1 = np.linspace(np.min(X[:,1]),np.max(X[:,1]),100)
Z0,Z1 = np.meshgrid(z0,z1)
V = np.vstack([Z0.ravel(),Z1.ravel()]).T
probabilityMap = np.double(np.reshape(classifier.predict(V),[len(z0),len(z1)]))
ax.contourf(z0,z1,probabilityMap,alpha=0.2,levels=[0,0.5],colors='k')
ax.contourf(z0,z1,probabilityMap,alpha=0.2,levels=[0.5,1],colors='r')
ax.set_xlabel('Feature 1',fontsize=16)
ax.set_ylabel('Feature 2',fontsize=16)

```

![png](output_10_1.png)

Yay! It works.


### Cats vs non-cats
Now let's do the task from the course: discriminating cats from non-cats. I've fetched the dataset from the Coursera Jupyterhub and the corresponding data module. I'm reproducing the number of iterations and the learning rate from the course, and we should expect to see >99% accuracy on the training data, and around 70% accuracy on the test data.


```python
from lr_utils import load_dataset

XTrainRaw,yTrain,XTestRaw,yTest,classes = load_dataset()
    
def flatten_arrays(X):
    allZ = [X[k,:,:,:].ravel() for k in range(X.shape[0])]
    
    return np.vstack(allZ)

XTrain = flatten_arrays(XTrainRaw)
XTest = flatten_arrays(XTestRaw)

yTrain,yTest = yTrain[0,:],yTest[0,:]

classifier = LogisticRegression(maxIterations=2000,learningRate=0.005)
classifier.fit(XTrain,yTrain)
yTrainHat = classifier.predict(XTrain)
yTestHat = classifier.predict(XTest)

trainAccuracy = np.mean(yTrain == yTrainHat)
testAccuracy = np.mean(yTest == yTestHat)

print('Train accuracy: %.2f'%trainAccuracy)
print('Test accuracy: %.2f'%testAccuracy)
```
    Train accuracy: 1.00
    Test accuracy: 0.74

And indeed, we're able to reproduce this with our logistic regression from scratch. Happy days.

### Why cross-entropy/log loss cost function?
This optional lecture was perhaps the most insightful piece of this entire week. I always welcome
the chance to practice implementing ML algorithms, but getting some real understanding of *why*
is always incredibly rewarding.

The reason why we want to use a cross-entropy cost function is as follows:

Basically, the logistic function will try to accomplish the following

$$
P(y|\mathbf{x}) = 
\begin{cases}
\hat{y} & \mbox{if } y = 1 \\\
1-\hat{y} & \mbox{if } y = 0.
\end{cases}
$$

We can rewrite this into a single equation

$$
P(y|\mathbf{x}) = \hat{y}^{y} (1-\hat{y})^{1-y}.
$$

This makes sense because when \\(y=1\\), we can see that \\((1-\hat{y})^{1-y} = 1\\) and
\\(\hat{y}^{y} = \hat{y}\\). Similarly, when \\(y=0\\), we find that the expression is simply
\\(1-\hat{y}\\). Assuming that the training data is independent and identically distributed,
we want to find the parameters which maximise the joint probability across all training examples.

$$
P(\mathbf{y} | X) = \prod_i^m P(y^{(i)} | \mathbf{x}^{(i)}).
$$

Maximising the log probability (or more appropriately, the log likelihood) is much more economical
since the product turns into a sum.

$$
\log P(\mathbf{y} | X) = \sum_i^m \log P(y^{(i)} | \mathbf{x}^{(i)})
$$
    
Plugging in our definition of \\( P(y \| \mathbf{x}) \\) above, we get
    
$$
\log P(\mathbf{y} | X) = \sum_i^m \log \left( (\hat{y}^{(i)})^{y^{(i)}}
     (1-\hat{y^{(i)}})^{1-y^{(i)}} \right).
$$

Because \\(\log a^b = b \log a\\) and \\( \log cd = \log c + \log d \\), the above reduces to

$$
\log P(\mathbf{y} | X) = \sum_i^m y^{(i)} \log{ \hat{y}^{(i)} } +
     (1-y^{(i)}) \log{(1-\hat{y}^{(i)})}.
$$

What this is really showing is that maximum likelihood estimation for logistic regression uses
the cross-entropy/log loss, and gives a very clear rationale for why this is the case.